﻿using ApiWeb.Entities;
using ApiWeb.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Contexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }


        public DbSet<Autor> Autores { get; set; }

        public DbSet<Libro> Libros { get; set; }

        //public DbSet<HostedServiceLog> HostedServiceLogs { get; set; }




    }
}
