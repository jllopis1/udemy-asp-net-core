﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Controllers
{
    [Route("api/values")]
    [ApiController]
    //[Authorize] //Con esta configuracion afecta a todos los endpoints
    public class ValuesController : ControllerBase
    {
        private readonly IConfiguration configuration;


        //Configuracion
        public ValuesController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }


        //GET api/Values

        [HttpGet]
        [ResponseCache(Duration = 15)]
        [Authorize]
        public ActionResult<string> Get()
        {
            return DateTime.Now.Second.ToString();
        }

        //GET api/value/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            //id++;
            //var b = id * 2;
            //return "value" + b.ToString();

            return configuration["apellido"];
        }


        //POST api/values 
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        //PUT api/values/5
        [HttpPut("{id}")]
        public void Put([FromBody] string value)
        {
        }


        //DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }
    }
}
