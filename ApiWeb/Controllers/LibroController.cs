﻿using ApiWeb.Contexts;
using ApiWeb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibroController : ControllerBase
    {

        private readonly ApplicationDbContext context;
        public LibroController(ApplicationDbContext context)
        {
            this.context = context;
        }



        [HttpGet]
        public ActionResult<IEnumerable<Libro>> GetAll() 
        {
            return context.Libros.Include(x => x.Autor).ToList();//include incluye la entidad relacionada a la entidad que queremos traer de la DB
        }

        [HttpGet("{id}", Name = "ObtenerLibro")]
        public ActionResult<Libro> GetLibro(int id)
        {
            var libro = context.Libros.FirstOrDefault(x => x.Id == id);

            if (libro == null)
            {
                return NotFound();
            }

            return libro;
        }


        [HttpPost]
        public ActionResult Post([FromBody] Libro libro)
        {

            context.Libros.Add(libro);

            context.SaveChanges();

            return new CreatedAtRouteResult("ObtenerLibro", new { id = libro.Id }, libro);
        }


        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody]Libro libro)
        {
            if (id != libro.Id)
            {
                return BadRequest();
            }

            context.Entry(libro).State = EntityState.Modified;

            context.SaveChanges();

            return Ok();
        }


        [HttpDelete("{id}")]
        public ActionResult<Libro> Delete(int id)
        {
            var libro = context.Libros.FirstOrDefault(x => x.Id == id);

            if (libro == null)
            {
                return NotFound();
            }

            context.Libros.Remove(libro);
            context.SaveChanges();
            return libro;
        }
    }
}
