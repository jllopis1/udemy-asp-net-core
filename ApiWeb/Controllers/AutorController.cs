﻿using ApiWeb.Contexts;
using ApiWeb.Entities;
using ApiWeb.Helpers;
using ApiWeb.Models;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutorController : ControllerBase
    {

        private readonly ApplicationDbContext context;

        private readonly ILogger<AutorController> logger;

        private readonly IMapper mapper;


 
        public AutorController(ApplicationDbContext context, ILogger<AutorController> logger, IMapper mapper) //Con esto podemos utilizar EF desde nuestro controlador
        {
            this.context = context;
            this.logger = logger;
            this.mapper = mapper;
        }


        [HttpGet("primerAutor")]
        public async Task<ActionResult<Autor>> GetFirstAutor()
        {
            var autor = await context.Autores.FirstOrDefaultAsync();

            return autor;
        }


        //GET api/autor/listado (descomenta el otro get)
        [HttpGet("/listado")]
        [HttpGet("listado")]
        [HttpGet]
        [ServiceFilter(typeof(MiFiltroDeAccion))]
        public ActionResult<IEnumerable<AutorDTO>> GetAll() 
        {
            // logger.LogInformation("Obteniendo lista de Autores");
            
            var autores =  context.Autores.Include(x => x.Libros).ToList();//Incluye los libros en la entidad Autor

            var autoresDTO = mapper.Map<List<AutorDTO>>(autores);

            return autoresDTO;
        }

        //GET api/autor/2/Pepe o GET api/autor/2
        [HttpGet("{id}/{param2?}", Name = "ObtenerAutor")]
        public async Task<ActionResult<AutorDTO>> GetAutor(int id, string param2)
        {
            var autor = await context.Autores.FirstOrDefaultAsync(x => x.Id == id);

            if (autor == null)
            {
                logger.LogWarning($"Autor de la Id {id} no ha sido encontrado");
                return NotFound();
            }

            var autorDTO = mapper.Map<AutorDTO>(autor);

            return autorDTO;
        }

        
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] AutorCreacionDTO autorCreacion)
        {
            var autor = mapper.Map<Autor>(autorCreacion);
            await context.Autores.AddAsync(autor);

            await context.SaveChangesAsync();

            var autorDTO = mapper.Map<AutorDTO>(autor);
            return new CreatedAtRouteResult("ObtenerAutor", new { id = autor.Id }, autorDTO);
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Patch(int id, [FromBody] JsonPatchDocument<AutorCreacionDTO> patchDocument )
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }

            
            var autor = await context.Autores.FirstOrDefaultAsync(x => x.Id == id);

            if (autor == null)
            {
                return NotFound();
            }
            var autorDTO = mapper.Map<AutorCreacionDTO>(autor);

            patchDocument.ApplyTo(autorDTO, ModelState);

            var isValid = TryValidateModel(autor);

            if (!isValid)
            {
                return BadRequest(ModelState);
            }

            await context.SaveChangesAsync();

            return NoContent();
        }





        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody]AutorCreacionDTO autorActualizacion)
        {
            var autor = mapper.Map<Autor>(autorActualizacion);
            autor.Id = id;
            context.Entry(autor).State = EntityState.Modified;
            await  context.SaveChangesAsync();

            return Ok();
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult<Autor>> Delete(int id)
        {
            //var autor = await context.Autores.FirstOrDefaultAsync(x => x.Id == id);
            var autorId = await context.Autores.Select(x => x.Id).FirstOrDefaultAsync(x => x == id); // hace que solo muestre el Id borrado por lo que la query es mas rapida

            if (autorId == default(int))
            {
                return NotFound();
            }

            //context.Autores.Remove(autor);
            context.Remove(new Autor {Id = autorId });
            await context.SaveChangesAsync();
            //return autorId;

            return NoContent();
        }
    }
}
