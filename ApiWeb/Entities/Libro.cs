﻿using ApiWeb.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Entities
{
    public class Libro
    {
        public int Id { get; set; }

        [Required]
        [PrimeraLetraMayuscula]
        public string Titulo {get; set;}

        [Required]
        public int AutorId { get; set; }

        public Autor Autor { get; set; }

    }
}
