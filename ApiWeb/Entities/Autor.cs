﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ApiWeb.Helpers;

namespace ApiWeb.Entities
{
    public class Autor  
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10,ErrorMessage ="El Nombre debe tener {1} caracteres o menos")]

        //[PrimeraLetraMayuscula]
        public string Nombre { get; set; }

        public List<Libro> Libros { get; set; }

        [Range(18, 120)]
        public int Edad { get; set; }

        [CreditCard]
        public string CreditCard { get; set; }

        [Url]
        public string Url { get; set; }
    }
}
