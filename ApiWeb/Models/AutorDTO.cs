﻿using ApiWeb.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Models
{
    public class AutorDTO : IValidatableObject
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "El Nombre debe tener {1} caracteres o menos")]

        //[PrimeraLetraMayuscula]
        public string Nombre { get; set; }

        public List<LibroDTO> Libros { get; set; }

        [Range(18, 120)]
        public int Edad { get; set; }

        [Url]
        public string Url { get; set; }

        //Validacion Por Modelo
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!string.IsNullOrEmpty(Nombre))
            {
                var firstLetter = Nombre[0].ToString();

                if (firstLetter != firstLetter.ToUpper())
                {
                    yield return new ValidationResult("La primera letra debe ser mayuscula", new string[] { nameof(Nombre) });
                }
            }

        }
    }
}
