﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Models
{
    public class AutorCreacionDTO
    {
        [Required]
        public string Nombre { get; set; }

        [Range(18, 120)]
        public int Edad { get; set; }
    }
}
